import { Component, OnInit } from '@angular/core';
import { Userlogindata } from '../userlogindata';
import { ScrumdataService } from '../scrumdata.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  scrumUserLoginData = new Userlogindata('', '', '');

  constructor(private _scrumdataService: ScrumdataService, private _router: Router) { }

  feedbk = '';

  ngOnInit(): void {
  }

  onLoginSubmit(){
    this._scrumdataService.login(this.scrumUserLoginData).subscribe(
      data => {
        console.log('Success!', data)
        localStorage.setItem('Authuser', JSON.stringify(this.scrumUserLoginData))
        localStorage.setItem('Authobj', JSON.stringify(data))
        localStorage.setItem('token', data.token)
        localStorage.setItem('name', data.name)
        localStorage.setItem('role', data.role)
        localStorage.setItem('name', data.role_id)
        // console.log('Success!', data.data)
        this._router.navigate(['/scrumboard', data['project_id']])
      },
      error => {
        console.log('Error!', error)
        this.feedbk = 'Log in failed! Please try again.'
      }
    )
  }

}

import { Component, OnInit } from '@angular/core';
import { Scrumuser } from '../scrumuser';
import { ScrumdataService } from '../scrumdata.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private _scrumdataService: ScrumdataService) {}

  ngOnInit(): void {
  }
  userTypes=['Developer', 'Owner'];
  scrumUserModel = new Scrumuser('john', 'johndoe@linuxjobber.com', 'testing123', 'normal user', '');

  successMessage = '';

  onSubmit(){
    console.log(this.scrumUserModel)
    this._scrumdataService.signup(this.scrumUserModel).subscribe(
      // data => this.successMessage = data,
      // error => console.error('Error!', error)
      data => {
        console.log('Success!', data)
        // localStorage.setItem('token', data.token)
        // this._router.navigate(['/scrumboard', data['project_id']])
      },
      error => {
        console.log('Error!', error)
        // this.feedbk = 'Log in failed! Please try again.'
      }
    )
    
  }

  

}
